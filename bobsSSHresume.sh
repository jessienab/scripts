#!/bin/bash

## Bobs SSH Resume
## by: Jason Nabein
## 
## Fake "terminal style" scrolling/selection resume or CV for SSH.
## Intended to run as a jailed/sftp style ssh app.


## start FUNCTIONS

# check everything is OK
checkok() {
	    if [ $? -eq 0 ]; then
		:
	else
		echo -e "Error during installation/configuration, please check error messages"
		exit 1
	fi
}

# root check. don't run as root!
if [ "$EUID" -eq 0 ]; then
  echo "This script should be run as a normal user, not sudo or root."
  exit 1
fi

## end FUNCTIONS

## start VARIABLES

CV1="Jason Nabein"

## end VARIABLES

## start INTRO
# Now these could be put into a separate text file to clean things up,
# but I want everything contained to a single file.

clear
echo "             ____        __   _          __________ __  __"
sleep 0.2
echo "            / __ )____  / /_ ( )_____   / ___/ ___// / / /"
sleep 0.2
echo "           / __  / __ \/ __ \|// ___/   \__ \\__ \ / /_/ /"
sleep 0.2
echo "          / /_/ / /_/ / /_/ / (__  )   ___/ /__/ / __  /  "
sleep 0.2
echo "         /_____/\____/_.___/ /____/   /____/____/_/ /_/   "
sleep 0.2
echo "                  ____                               		"
sleep 0.2
echo "                 / __ \___  _______  ______ ___  ___ 		"
sleep 0.2
echo "                / /_/ / _ \/ ___/ / / / __  __ \/ _ \		"
sleep 0.2
echo "               / _, _/  __(__  ) /_/ / / / / / /  __/		"
sleep 0.2
echo "              /_/ |_|\___/____/\__,_/_/ /_/ /_/\___/ 		"

sleep 1.7

echo "				____________________						"
sleep 0.2
echo "			   	< It's Pretty Great! >						"
sleep 0.2
echo "				--------------------						" 
sleep 0.2
echo "				       \   ^__^								"
sleep 0.2
echo "				        \  (..)\_______						"
sleep 0.2
echo "				           (__)\       )\/\					"
sleep 0.2
echo "				               ||----w |					"
sleep 0.2
echo "				               ||     ||					"
sleep 1.7

## end INTRO

## start SELECTION
clear

printf "Welcome to the SSH Resume system! \nPlease select the resume you want to view: \n"
sleep 0.2
#I want these to print off at like 0.05 speed sequentially...
seq 20 | xargs -I -- printf "="
echo ""
sleep 0.2
echo "[1] $CV1"
sleep 0.2
#I want these to print off at like 0.05 speed sequentially...
seq 20 | xargs -I -- printf "="
echo ""

