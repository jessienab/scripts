#!/bin/bash
BASEDIR=""

pushd "$BASEDIR" 2>&1 >/dev/null

for i in */ ; do

	#Change into initial directory
        pushd "$i"

	# Set vars for next command
	# https://stackoverflow.com/a/50634869
        IFS=/
        cdir=($PWD)
		
		# This is used to ignore/skip over specific desired folders
		# that may not have upstream sources; example, git repos hosting
		# static files, only as a means of tracking updates
		$IGNOREDIR1=misc

                if [ ${cdir[-1]} == $IGNOREDIR1 ] ; then
                        popd 2>&1 >/dev/null
                else
                        git pull && popd 2>&1 >/dev/null
                fi

done
